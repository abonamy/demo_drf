# Configure virtual environment  
`python3 -m venv pyve`  
`source pyve/bin/activate`  
`python --version`  (3.7.4)  


# Add packages  
`pip install --upgrade pip`  
`pip install Django==2.2.4`  
`pip install djangorestframework`  
`pip install markdown`  
`pip install django-filter`  
`pip install flake8`  
`pip freeze > requirements.txt`  

`pip list`  

```
Package             Version
------------------- -------
Django              2.2.4
djangorestframework 3.11.0
Markdown            3.1.1
pip                 19.3.1
pytz                2019.3
setuptools          40.8.0
sqlparse            0.3.0
```


# Setup api_cf project  
`django-admin startproject api_cf`  
`python manage.py migrate`  

`python manage.py createsuperuser`  

`python manage.py runserver`  


# Add monitoring application to api_cf project  
`python manage.py startapp monitoring`  

`python manage.py makemigrations monitoring`  
`python manage.py showmigrations`  
`python manage.py sqlmigrate monitoring 0001`  
`python manage.py migrate`  
