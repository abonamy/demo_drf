from django.db import models


class Farmer(models.Model):
    name = models.CharField(max_length=42)
    siret = models.CharField(max_length=42)
    address = models.CharField(max_length=42)

    def __str__(self):
        return "%s" % (self.name,)


class Product(models.Model):
    name = models.CharField(max_length=42)
    unit = models.CharField(max_length=42)
    intl_code = models.CharField(max_length=42)
    producers = models.ManyToManyField(Farmer)

    def __str__(self):
        return "{name}".format(name=self.name)


class Certificate(models.Model):
    CATEGORIES = (
        ('B', 'Biological'),
        ('G', 'GMO-free'),
        ('O', 'Origin'),
    )
    name = models.CharField(max_length=42)
    category = models.CharField(max_length=1, choices=CATEGORIES, default='B')
    certified_farmer = models.ForeignKey(Farmer, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}"
