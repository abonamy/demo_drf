from rest_framework import serializers

from .models import Farmer, Product, Certificate


class FarmerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Farmer
        fields = ['name', 'siret', 'address']


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'unit', 'intl_code', 'producers']


class CertificateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Certificate
        fields = ['name', 'category', 'certified_farmer']
