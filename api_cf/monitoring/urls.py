from django.urls import include, path
from rest_framework import routers

from .views import FarmerViewSet, ProductViewSet, CertificateViewSet


router = routers.DefaultRouter()
router.register(r'farmers', FarmerViewSet)
router.register(r'products', ProductViewSet)
router.register(r'certificates', CertificateViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
